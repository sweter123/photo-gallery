package pl.sweter

import android.app.Application
import pl.sweter.photogallery.di.AppComponent
import pl.sweter.photogallery.di.DaggerAppComponent
import pl.sweter.photogallery.di.modules.NetworkModule
import timber.log.Timber

class App : Application() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent
            .builder()
            .networkModule(NetworkModule())
            .build()

        Timber.plant(Timber.DebugTree())


    }

}