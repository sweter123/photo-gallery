package pl.sweter.photogallery.ui.list

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import pl.sweter.App
import pl.sweter.photogallery.R
import pl.sweter.photogallery.ui.list.adapter.OnItemClickListener
import pl.sweter.photogallery.ui.list.adapter.PhotoListAdapter
import pl.sweter.photogallery.ui.model.UiPhoto
import timber.log.Timber
import javax.inject.Inject

class PhotoListFragment : Fragment(), OnItemClickListener {

  private val listViewModel by lazy { ViewModelProviders.of(this, viewModelFactory)[ListViewModel::class.java] }

  private val adapter by lazy { PhotoListAdapter(this) }

  @BindView(R.id.photos_recycler)
  lateinit var recyclerView: RecyclerView

  @BindView(R.id.query_edit_text)
  lateinit var query: EditText

  private lateinit var disposables: CompositeDisposable

  @Inject
  lateinit var viewModelFactory: ViewModelProvider.Factory

  @Inject
  lateinit var textWatcher: QueryTextWatcher

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
    inflater.inflate(R.layout.fragment_photo_list, container, false).apply {
      ButterKnife.bind(this@PhotoListFragment, this)
    }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    App.appComponent.inject(this@PhotoListFragment)
    recyclerView = view.findViewById(R.id.photos_recycler)
    recyclerView.adapter = adapter
    val spanCount = if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) 4 else 2
    recyclerView.layoutManager = GridLayoutManager(context, spanCount)
      .apply {
        spanSizeLookup = object : SpanSizeLookup() {
          override fun getSpanSize(position: Int): Int {
            return if (adapter.isListStateItem(position)) spanCount
            else 1
          }
        }
      }
    listViewModel.photoList
      .observe(this@PhotoListFragment, Observer { adapter.submitList(it) })
    listViewModel.listLoadingState
      .observe(this@PhotoListFragment, Observer { adapter.loadingState = it })
    query.addTextChangedListener(textWatcher)
  }

  override fun onResume() {
    super.onResume()
    disposables = CompositeDisposable()
    disposables += textWatcher.getChanges()
      .subscribe {
        listViewModel.setQuery(it)
        Timber.d("Query: $it")
      }

  }

  override fun onPause() {
    super.onPause()
    disposables.dispose()
  }

  override fun onPhotoClicked(photo: UiPhoto) {
    val direction = PhotoListFragmentDirections.photoDetails(photo)
    findNavController().navigate(direction)
  }

  override fun onErrorStateItemClick(loadingState: ListLoadingState) {
    listViewModel.retry()
  }
}
