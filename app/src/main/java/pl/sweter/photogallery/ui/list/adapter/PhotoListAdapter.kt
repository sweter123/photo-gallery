package pl.sweter.photogallery.ui.list.adapter

import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import pl.sweter.photogallery.ui.model.UiPhoto
import pl.sweter.photogallery.ui.list.ListLoadingState
import pl.sweter.photogallery.ui.list.ListLoadingState.LOADED
import pl.sweter.photogallery.util.delegates.PhotoAdapterDelegateManager

class PhotoListAdapter(private val onClickListener: OnItemClickListener) :
  PagedListAdapter<UiPhoto, ViewHolder>(DIFF_CALLBACK) {

  private val delegateManager = PhotoAdapterDelegateManager().apply {
    addDelegate(PhotoDelegate(onClickListener))
    addDelegate(ErrorDelegate(onClickListener))
    addDelegate(EmptyListDelegate())
    addDelegate(LoadingDelegate())
  }

  var loadingState: ListLoadingState? = null
    set(value) {
      val previousState = loadingState
      val hadExtraRow = hasExtraRow()
      field = value
      if (hasExtraRow() != hadExtraRow) {
        if (hadExtraRow) {
          notifyItemRemoved(super.getItemCount())
        } else {
          notifyItemInserted(super.getItemCount())
        }
      } else if (hasExtraRow() && previousState != value) {
        notifyItemChanged(itemCount)
      }
    }

  companion object {
    val DIFF_CALLBACK = object : DiffUtil.ItemCallback<UiPhoto>() {
      override fun areItemsTheSame(oldItem: UiPhoto, newItem: UiPhoto) =
        newItem.id == oldItem.id

      override fun areContentsTheSame(oldItem: UiPhoto, newItem: UiPhoto) =
        newItem.hashCode() == oldItem.hashCode()

    }
  }

  override fun getItem(position: Int): UiPhoto? =
    if (position < super.getItemCount()) super.getItem(position) else null

  override fun getItemViewType(position: Int): Int {
    return delegateManager.getItemViewType(getItem(position), loadingState)
  }

  fun isListStateItem(position: Int) = hasExtraRow() && position == itemCount - 1

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = delegateManager.onCreateViewHolder(parent, viewType)

  override fun onBindViewHolder(holder: ViewHolder, position: Int) =
    delegateManager.onBindViewHolder(holder, getItem(position), loadingState)

  override fun getItemCount(): Int {
    return if (hasExtraRow()) super.getItemCount() + 1 else super.getItemCount()
  }

  private fun hasExtraRow() = loadingState != null && loadingState != LOADED

}