package pl.sweter.photogallery.ui.list

import androidx.annotation.StringRes
import androidx.paging.PagedList
import pl.sweter.photogallery.ui.model.UiPhoto

sealed class PhotoListViewState {

  data class LoadedViewState(val photos: PagedList<UiPhoto>?) : PhotoListViewState()

  object LoadingViewState : PhotoListViewState()

  class InitialErrorViewState(@StringRes val message: Int) : PhotoListViewState()

  class NextPageErrorViewState(@StringRes val message: Int) : PhotoListViewState()

}