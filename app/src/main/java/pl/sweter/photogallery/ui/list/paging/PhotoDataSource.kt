package pl.sweter.photogallery.ui.list.paging

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import pl.sweter.photogallery.ui.model.UiPhoto
import pl.sweter.photogallery.repository.mappers.mapToLocal
import pl.sweter.photogallery.repository.remote.ApiException
import pl.sweter.photogallery.repository.remote.NetworkPhotoRepository
import pl.sweter.photogallery.ui.list.ListLoadingState
import pl.sweter.photogallery.ui.list.ListLoadingState.INITIAL_ERROR
import pl.sweter.photogallery.ui.list.ListLoadingState.LOADED
import pl.sweter.photogallery.ui.list.ListLoadingState.LOADED_EMPTY
import pl.sweter.photogallery.ui.list.ListLoadingState.LOADING
import pl.sweter.photogallery.ui.list.ListLoadingState.NEXT_ERROR

class PhotoDataSource(
  private val repository: NetworkPhotoRepository,
  private val query: String?
) : PageKeyedDataSource<Long, UiPhoto>() {

  private val compositeDisposable = CompositeDisposable()

  val listLoadingState: MutableLiveData<ListLoadingState> = MutableLiveData()

  var retry: (() -> Unit)? = null

  override fun loadInitial(params: LoadInitialParams<Long>, callback: LoadInitialCallback<Long, UiPhoto>) {
    listLoadingState.postValue(LOADING)
    compositeDisposable += repository.getPhotos(1, params.requestedLoadSize, query)
      .subscribeOn(Schedulers.io())
      .map { it.map { photo -> photo.mapToLocal() } }
      .observeOn(AndroidSchedulers.mainThread())
      .subscribeBy(
        onSuccess = {
          if (it.isNotEmpty()) {
            listLoadingState.value = LOADED
            callback.onResult(it, null, 2)
          } else {
            listLoadingState.value = LOADED_EMPTY
          }
        },
        onError = {
          retry = {
            loadInitial(params, callback)
          }
          listLoadingState.value = INITIAL_ERROR
        }
      )
  }

  override fun loadAfter(params: LoadParams<Long>, callback: LoadCallback<Long, UiPhoto>) {
    listLoadingState.postValue(LOADING)
    compositeDisposable += repository.getPhotos(params.key, params.requestedLoadSize, query)
      .subscribeOn(Schedulers.io())
      .map { it.map { photo -> photo.mapToLocal() } }
      .observeOn(AndroidSchedulers.mainThread())
      .subscribeBy(
        onSuccess = {
          listLoadingState.value = LOADED
          callback.onResult(it, params.key + 1)
        },
        onError = {
          if (it is ApiException && it.code == 400) {
            listLoadingState.value = LOADED
          } else {
            retry = {
              loadAfter(params, callback)
            }
            listLoadingState.value = NEXT_ERROR
          }
        }
      )
  }

  override fun loadBefore(params: LoadParams<Long>, callback: LoadCallback<Long, UiPhoto>) {
    listLoadingState.postValue(LOADING)
    compositeDisposable += repository.getPhotos(params.key, params.requestedLoadSize, query)
      .subscribeOn(Schedulers.io())
      .map { it.map { photo -> photo.mapToLocal() } }
      .observeOn(AndroidSchedulers.mainThread())
      .subscribeBy(
        onSuccess = { callback.onResult(it, params.key - 1) },
        onError = {
          retry = {
            loadBefore(params, callback)
          }
          listLoadingState.value = NEXT_ERROR
        }
      )
  }

}