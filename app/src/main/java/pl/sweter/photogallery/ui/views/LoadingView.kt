package pl.sweter.photogallery.ui.views

import android.animation.ValueAnimator
import android.animation.ValueAnimator.INFINITE
import android.animation.ValueAnimator.RESTART
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.core.content.ContextCompat
import pl.sweter.photogallery.R

class LoadingView @JvmOverloads constructor(
  context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

  init {
    ValueAnimator.ofFloat(0f, (2 * Math.PI).toFloat()).apply {
      repeatMode = RESTART
      repeatCount = INFINITE
      duration = 1000
      addUpdateListener {
        time = (it.animatedValue as Float).toDouble()
        postInvalidate()
      }
      interpolator = AccelerateDecelerateInterpolator()
      start()
    }
  }

  var rectCount = 5
  var rectCornerRadius = 16f
  private var animThreshold = Math.PI / rectCount

  private var time = 0.0

  private val paint by lazy {
    Paint().apply {
      color = ContextCompat.getColor(context, R.color.colorPrimaryDark)
    }
  }

  private val rects = mutableListOf<RectF>()

  private fun initRects(canvasWidth: Int, height: Int) {
    val rectWidth: Float = canvasWidth.toFloat() / (rectCount + ((rectCount + 1f) / 2))
    val rectSpacing = rectWidth / 2
    rects.clear()
    repeat(rectCount) { count ->
      val left = rectSpacing * (count + 1) + rectWidth * count
      val right = rectSpacing * (count + 1) + rectWidth * (count + 1)
      val top = rectSpacing
      val bottom = height - rectSpacing
      rects.add(RectF(left, top, right, bottom))
    }
  }

  override fun onDraw(canvas: Canvas) {
    super.onDraw(canvas)
    if (rects.isEmpty()) {
      initRects(width, height)
    }
    rects.forEachIndexed { index, rectF ->
      canvas.drawScaledRoundRect(rectF, rectCornerRadius, paint, (Math.cos(time + (animThreshold * index)).toFloat() + 1) / 2)
    }
  }

  private fun Canvas.drawScaledRoundRect(rect: RectF, rectCornerRadius: Float, paint: Paint, scale: Float) {
    val rectWidth = rect.right - rect.left
    val rectHeight = rect.bottom - rect.top
    val newRect = RectF(rect)
    newRect.left += (rectWidth / 2) * (1 - scale)
    newRect.right -= (rectWidth / 2) * (1 - scale)
    newRect.top += (rectHeight / 2) * (1 - scale)
    newRect.bottom -= (rectHeight / 2) * (1 - scale)
    val scaledRadius = rectCornerRadius * scale
    drawRoundRect(newRect, scaledRadius, scaledRadius, paint)
  }

}