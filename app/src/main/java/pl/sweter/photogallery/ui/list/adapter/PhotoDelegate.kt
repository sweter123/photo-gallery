package pl.sweter.photogallery.ui.list.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import pl.sweter.photogallery.R
import pl.sweter.photogallery.ui.model.UiPhoto
import pl.sweter.photogallery.ui.list.ListLoadingState
import pl.sweter.photogallery.util.bindView
import pl.sweter.photogallery.util.delegates.PhotoAdapterDelegate

class PhotoDelegate(private val onItemClickListener: OnItemClickListener) : PhotoAdapterDelegate {

  override fun onCreateViewHolder(parent: ViewGroup): ViewHolder = PhotoViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.photo_list_item, parent, false)
  )

  override fun onBindViewHolder(holder: ViewHolder, listLoadingState: ListLoadingState?, item: UiPhoto?) {
    holder as PhotoViewHolder
    item?.apply {
      Glide.with(holder.image)
        .load(thumbnailUrl)
        .transition(DrawableTransitionOptions.withCrossFade())
        .apply(RequestOptions.centerCropTransform())
        .into(holder.image)

      Glide.with(holder.image)
        .load(userImageUrl)
        .apply(RequestOptions.circleCropTransform())
        .transition(DrawableTransitionOptions.withCrossFade())
        .into(holder.avatar)
      holder.userName.text = user
      holder.likes.text = likes.toString()
      holder.itemView.setOnClickListener { onItemClickListener.onPhotoClicked(item) }
    }
  }

  override fun isForViewType(item: UiPhoto?, listLoadingState: ListLoadingState?): Boolean = item != null

  class PhotoViewHolder(itemView: View) : ViewHolder(itemView) {
    val image: ImageView by bindView(R.id.image)
    val avatar: ImageView by bindView(R.id.user_avatar)
    val userName: TextView by bindView(R.id.user_name)
    val likes: TextView by bindView(R.id.like_count)
  }
}