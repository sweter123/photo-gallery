package pl.sweter.photogallery.ui.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import io.reactivex.disposables.CompositeDisposable
import pl.sweter.photogallery.repository.remote.NetworkPhotoRepository
import pl.sweter.photogallery.ui.list.paging.PhotoDataSourceFactory
import java.util.concurrent.Executors
import javax.inject.Inject

class ListViewModel @Inject constructor(
  networkPhotoRepository: NetworkPhotoRepository
) : ViewModel() {

  private val compositeDisposable = CompositeDisposable()
  private val photoDataFactory = PhotoDataSourceFactory(networkPhotoRepository)

  private val pagedListConfig = PagedList.Config.Builder()
    .setEnablePlaceholders(false)
    .setInitialLoadSizeHint(10)
    .setPageSize(20).build()

  //LiveDatas
  val photoList = LivePagedListBuilder(photoDataFactory, pagedListConfig)
    .setFetchExecutor(Executors.newFixedThreadPool(5))
    .build()
  val listLoadingState: LiveData<ListLoadingState> = photoDataFactory.listLoadingState

  fun setQuery(query: String) {
    photoDataFactory.query = query
  }

  fun retry() = photoDataFactory.retry()

  override fun onCleared() {
    compositeDisposable.dispose()
    super.onCleared()
  }

}