package pl.sweter.photogallery.ui.list

import android.content.res.Configuration
import android.os.Bundle
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import pl.sweter.App
import pl.sweter.photogallery.R
import pl.sweter.photogallery.ui.model.UiPhoto
import pl.sweter.photogallery.ui.list.adapter.OnItemClickListener
import pl.sweter.photogallery.ui.list.adapter.PhotoListAdapter
import pl.sweter.photogallery.util.bindView
import timber.log.Timber
import javax.inject.Inject

class PhotoListActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_photo_list)
  }

}
