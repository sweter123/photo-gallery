package pl.sweter.photogallery.ui.list.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import pl.sweter.photogallery.R
import pl.sweter.photogallery.ui.model.UiPhoto
import pl.sweter.photogallery.ui.list.ListLoadingState
import pl.sweter.photogallery.ui.list.ListLoadingState.LOADED_EMPTY
import pl.sweter.photogallery.util.delegates.PhotoAdapterDelegate

class EmptyListDelegate : PhotoAdapterDelegate {

  override fun onCreateViewHolder(parent: ViewGroup): ViewHolder =
    EmptyListViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.empty_list_item, parent, false))

  override fun onBindViewHolder(holder: ViewHolder, listLoadingState: ListLoadingState?, item: UiPhoto?) {
    holder as EmptyListViewHolder
  }

  override fun isForViewType(item: UiPhoto?, listLoadingState: ListLoadingState?): Boolean =
    item == null && listLoadingState != null && listLoadingState == LOADED_EMPTY

  class EmptyListViewHolder(itemView: View) : ViewHolder(itemView)
}