package pl.sweter.photogallery.ui.list

import android.text.Editable
import android.text.TextWatcher
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit.MILLISECONDS
import javax.inject.Inject

class QueryTextWatcher @Inject constructor(): TextWatcher {

  private val changesSubject = PublishSubject.create<String>()

  override fun afterTextChanged(s: Editable?) = Unit

  override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

  override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    if(s != null){
      changesSubject.onNext(s.toString())
    }
  }

  fun getChanges(): Observable<String> = changesSubject
    .debounce(500, MILLISECONDS)

}