package pl.sweter.photogallery.ui.model

import android.os.Parcel
import android.os.Parcelable
import android.os.Parcelable.Creator

data class UiPhoto(
  val id: Long,
  val thumbnailUrl: String,
  val hdUrl: String,
  val views: Long,
  val likes: Long,
  val downloads: Long,
  val user: String,
  val imageSize: Long,
  val userImageUrl: String,
  val tags: String
): Parcelable {
  constructor(parcel: Parcel) : this(
    parcel.readLong(),
    parcel.readString()!!,
    parcel.readString()!!,
    parcel.readLong(),
    parcel.readLong(),
    parcel.readLong(),
    parcel.readString()!!,
    parcel.readLong(),
    parcel.readString()!!,
    parcel.readString()!!
  )

  override fun writeToParcel(parcel: Parcel, flags: Int) {
    parcel.writeLong(id)
    parcel.writeString(thumbnailUrl)
    parcel.writeString(hdUrl)
    parcel.writeLong(views)
    parcel.writeLong(likes)
    parcel.writeLong(downloads)
    parcel.writeString(user)
    parcel.writeLong(imageSize)
    parcel.writeString(userImageUrl)
    parcel.writeString(tags)
  }

  override fun describeContents(): Int {
    return 0
  }

  companion object CREATOR : Creator<UiPhoto> {
    override fun createFromParcel(parcel: Parcel): UiPhoto {
      return UiPhoto(parcel)
    }

    override fun newArray(size: Int): Array<UiPhoto?> {
      return arrayOfNulls(size)
    }
  }
}