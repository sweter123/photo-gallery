package pl.sweter.photogallery.ui.list.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import pl.sweter.photogallery.R
import pl.sweter.photogallery.ui.model.UiPhoto
import pl.sweter.photogallery.ui.list.ListLoadingState
import pl.sweter.photogallery.ui.list.ListLoadingState.INITIAL_ERROR
import pl.sweter.photogallery.ui.list.ListLoadingState.NEXT_ERROR
import pl.sweter.photogallery.util.bindView
import pl.sweter.photogallery.util.delegates.PhotoAdapterDelegate
import java.lang.NullPointerException

class ErrorDelegate(private val onItemClickListener: OnItemClickListener) : PhotoAdapterDelegate {
  override fun onCreateViewHolder(parent: ViewGroup): ViewHolder =
    ErrorViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.error_item, parent, false))

  override fun onBindViewHolder(holder: ViewHolder, listLoadingState: ListLoadingState?, item: UiPhoto?) {
    holder as ErrorViewHolder
    val context = holder.itemView.context
    holder.itemView.setOnClickListener { onItemClickListener.onErrorStateItemClick(listLoadingState!!) }
    when (listLoadingState) {
      INITIAL_ERROR -> holder.errorMessage.text = context.getString(R.string.error_message)
      NEXT_ERROR -> holder.errorMessage.text = context.getString(R.string.next_page_error_message)
      else -> NullPointerException("List loading state improper on ErrorDelegate, This shouldn't happen")
    }
  }

  override fun isForViewType(item: UiPhoto?, listLoadingState: ListLoadingState?): Boolean =
    item == null && listLoadingState != null &&
        (listLoadingState == INITIAL_ERROR || listLoadingState == NEXT_ERROR)

  class ErrorViewHolder(itemView: View) : ViewHolder(itemView) {
    val errorMessage: TextView by bindView(R.id.error_message)
  }
}