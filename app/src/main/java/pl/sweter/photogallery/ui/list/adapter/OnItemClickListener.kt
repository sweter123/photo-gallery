package pl.sweter.photogallery.ui.list.adapter

import pl.sweter.photogallery.ui.model.UiPhoto
import pl.sweter.photogallery.ui.list.ListLoadingState

interface OnItemClickListener {
  fun onPhotoClicked(photo: UiPhoto)
  fun onErrorStateItemClick(loadingState: ListLoadingState)
}