package pl.sweter.photogallery.ui.list.paging

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import pl.sweter.photogallery.ui.model.UiPhoto
import pl.sweter.photogallery.repository.remote.NetworkPhotoRepository
import pl.sweter.photogallery.ui.list.ListLoadingState
import pl.sweter.photogallery.util.switchMap

class PhotoDataSourceFactory(private val repository: NetworkPhotoRepository) :
  DataSource.Factory<Long, UiPhoto>() {

  private var dataSource: MutableLiveData<PhotoDataSource> = MutableLiveData()

  val listLoadingState: LiveData<ListLoadingState> = dataSource.switchMap { it.listLoadingState }

  var query: String? = null
    set(value) {
      field = value
      dataSource.value?.invalidate()
    }

  fun retry() = dataSource.value?.retry?.invoke()

  override fun create(): DataSource<Long, UiPhoto> {
    val dataSource = PhotoDataSource(repository, query)
    this.dataSource.postValue(dataSource)
    return dataSource
  }

}