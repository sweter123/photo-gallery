package pl.sweter.photogallery.ui.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import butterknife.BindView
import butterknife.ButterKnife
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.github.chrisbanes.photoview.PhotoView
import pl.sweter.photogallery.R
import pl.sweter.photogallery.ui.model.UiPhoto

class PhotoDetailsFragment : Fragment() {

  @BindView(R.id.details_image)
  lateinit var image: PhotoView

  @BindView(R.id.like_count)
  lateinit var likeCount: TextView

  @BindView(R.id.view_count)
  lateinit var viewCount: TextView

  @BindView(R.id.tags)
  lateinit var tags: TextView

  @BindView(R.id.user_name)
  lateinit var userName: TextView

  @BindView(R.id.downloads)
  lateinit var downloads: TextView

  @BindView(R.id.user_avatar)
  lateinit var userAvatar: ImageView

  private lateinit var photo: UiPhoto

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    return inflater.inflate(R.layout.fragment_details, container, false).apply {
      ButterKnife.bind(this@PhotoDetailsFragment, this)
    }
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    photo = PhotoDetailsFragmentArgs.fromBundle(arguments!!).photo
    image.maximumScale = 10f
    likeCount.text = photo.likes.toString()
    viewCount.text = photo.views.toString()
    tags.text = photo.tags
    userName.text = photo.user
    downloads.text = photo.downloads.toString()
    Glide.with(this)
      .load(photo.userImageUrl)
      .apply(RequestOptions.circleCropTransform())
      .transition(DrawableTransitionOptions.withCrossFade())
      .into(userAvatar)
    Glide.with(this)
      .load(photo.hdUrl)
      .into(image)
  }

}