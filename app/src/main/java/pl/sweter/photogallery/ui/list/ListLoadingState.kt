package pl.sweter.photogallery.ui.list

enum class ListLoadingState {
  LOADING, LOADED, LOADED_EMPTY, INITIAL_ERROR, NEXT_ERROR
}