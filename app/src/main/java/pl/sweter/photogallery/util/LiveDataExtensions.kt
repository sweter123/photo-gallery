package pl.sweter.photogallery.util

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations

fun <X, Y> LiveData<X>.map(block: (variable: X) -> Y): LiveData<Y> = Transformations.map(this, block)

fun <X, Y> LiveData<X>.switchMap(block: (variable: X) -> LiveData<Y>): LiveData<Y> = Transformations.switchMap(this, block)