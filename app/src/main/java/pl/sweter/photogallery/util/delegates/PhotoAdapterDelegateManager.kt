package pl.sweter.photogallery.util.delegates

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import pl.sweter.photogallery.ui.model.UiPhoto
import pl.sweter.photogallery.ui.list.ListLoadingState

class PhotoAdapterDelegateManager {

  private val delegates: MutableList<PhotoAdapterDelegate> = mutableListOf()

  fun addDelegate(delegate: PhotoAdapterDelegate) {
    delegates.add(delegate)
  }

  fun getItemViewType(item: UiPhoto?, listLoadingState: ListLoadingState?): Int {
    delegates.forEachIndexed { index, adapterDelegate ->
      if (adapterDelegate.isForViewType(item, listLoadingState)) {
        return index
      }
    }
    throw RuntimeException("No proper delegate found")
  }

  fun onBindViewHolder(holder: ViewHolder, item: UiPhoto?, listLoadingState: ListLoadingState?) =
    delegates[getItemViewType(item, listLoadingState)].onBindViewHolder(holder, listLoadingState, item)

  fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = delegates[viewType].onCreateViewHolder(parent)

}