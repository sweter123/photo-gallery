package pl.sweter.photogallery.util.delegates

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import pl.sweter.photogallery.ui.model.UiPhoto
import pl.sweter.photogallery.ui.list.ListLoadingState

interface PhotoAdapterDelegate {

  fun onCreateViewHolder(parent: ViewGroup): ViewHolder
  fun onBindViewHolder(holder: ViewHolder, listLoadingState: ListLoadingState?, item: UiPhoto?)
  fun isForViewType(item: UiPhoto?, listLoadingState: ListLoadingState?): Boolean

}