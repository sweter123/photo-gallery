package pl.sweter.photogallery.di

import dagger.Component
import pl.sweter.photogallery.di.modules.NetworkModule
import pl.sweter.photogallery.di.modules.ViewModelModule
import pl.sweter.photogallery.ui.list.PhotoListFragment
import javax.inject.Singleton

@Component(modules = [NetworkModule::class, ViewModelModule::class])
@Singleton
interface AppComponent {

  fun inject(photoListActivity: PhotoListFragment)

}