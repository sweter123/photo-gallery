package pl.sweter.photogallery.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import pl.sweter.photogallery.di.ViewModelFactory
import pl.sweter.photogallery.di.ViewModelKey
import pl.sweter.photogallery.ui.list.ListViewModel

@Module
abstract class ViewModelModule {

  @Binds
  internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

  @Binds
  @IntoMap
  @ViewModelKey(ListViewModel::class)
  internal abstract fun listViewModel(viewModel: ListViewModel): ViewModel

}