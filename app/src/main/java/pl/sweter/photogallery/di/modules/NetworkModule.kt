package pl.sweter.photogallery.di.modules

import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import pl.sweter.photogallery.BuildConfig
import pl.sweter.photogallery.repository.remote.PhotosService
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class NetworkModule {

  @Provides
  @Singleton
  fun provideImagesRepository(retrofit: Retrofit) = retrofit.create(PhotosService::class.java)

  @Provides
  @Singleton
  fun provideRetrofit(
    httpClient: OkHttpClient,
    converterFactory: Converter.Factory,
    callAdapterFactory: CallAdapter.Factory
  ) = Retrofit.Builder()
      .baseUrl(BuildConfig.BASE_ENDPOINT)
      .client(httpClient)
      .addConverterFactory(converterFactory)
      .addCallAdapterFactory(callAdapterFactory)
      .build()

  @Provides
  @Singleton
  fun provideOkHttp(loggingInterceptor: HttpLoggingInterceptor) = OkHttpClient.Builder()
      .readTimeout(10, TimeUnit.SECONDS)
      .callTimeout(10, TimeUnit.SECONDS)
      .connectTimeout(10, TimeUnit.SECONDS)
      .addInterceptor(loggingInterceptor)
      .build()

  @Provides
  @Singleton
  fun provideHttpInterceptor() =
    HttpLoggingInterceptor().apply {
      level = HttpLoggingInterceptor.Level.BODY
    }

  @Provides
  @Singleton
  fun provideGsonFactory(): Converter.Factory = GsonConverterFactory.create()

  @Provides
  @Singleton
  fun provideRxCallAdapterFactory(): CallAdapter.Factory =
    RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io())

}