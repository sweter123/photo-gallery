package pl.sweter.photogallery.repository.remote.model
import com.google.gson.annotations.SerializedName


data class NetworkPhotoList(
    @SerializedName("hits")
    val hits: List<NetworkPhoto>,
    @SerializedName("total")
    val total: Int,
    @SerializedName("totalHits")
    val totalHits: Int
)