package pl.sweter.photogallery.repository.mappers

import pl.sweter.photogallery.ui.model.UiPhoto
import pl.sweter.photogallery.repository.remote.model.NetworkPhoto

fun NetworkPhoto.mapToLocal() =
  UiPhoto(
    id = id,
    thumbnailUrl = webformatURL,
    hdUrl = largeImageURL,
    views = views,
    likes = likes,
    downloads = downloads,
    user = user,
    imageSize = imageSize,
    userImageUrl = userImageURL,
    tags = tags
  )