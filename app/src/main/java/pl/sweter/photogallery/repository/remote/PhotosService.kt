package pl.sweter.photogallery.repository.remote

import io.reactivex.Single
import pl.sweter.photogallery.repository.remote.model.NetworkPhotoList
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface PhotosService {

  @GET(".")
  fun getPhotos(
    @Query("key") apiKey: String,
    @Query("lang") langCode: String,
    @Query("page") page: Long,
    @Query("per_page") pageSize: Int,
    @Query("q") query: String?): Single<Response<NetworkPhotoList>>

}