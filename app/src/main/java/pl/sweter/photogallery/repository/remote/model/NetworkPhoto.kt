package pl.sweter.photogallery.repository.remote.model
import com.google.gson.annotations.SerializedName


data class NetworkPhoto(
    @SerializedName("comments")
    val comments: Int,
    @SerializedName("downloads")
    val downloads: Long,
    @SerializedName("favorites")
    val favorites: Int,
    @SerializedName("fullHDURL")
    val fullHDURL: String,
    @SerializedName("id")
    val id: Long,
    @SerializedName("imageHeight")
    val imageHeight: Int,
    @SerializedName("imageSize")
    val imageSize: Long,
    @SerializedName("imageURL")
    val imageURL: String,
    @SerializedName("imageWidth")
    val imageWidth: Int,
    @SerializedName("largeImageURL")
    val largeImageURL: String,
    @SerializedName("likes")
    val likes: Long,
    @SerializedName("pageURL")
    val pageURL: String,
    @SerializedName("previewHeight")
    val previewHeight: Int,
    @SerializedName("previewURL")
    val previewURL: String,
    @SerializedName("previewWidth")
    val previewWidth: Int,
    @SerializedName("tags")
    val tags: String,
    @SerializedName("type")
    val type: String,
    @SerializedName("user")
    val user: String,
    @SerializedName("userImageURL")
    val userImageURL: String,
    @SerializedName("user_id")
    val userId: Int,
    @SerializedName("views")
    val views: Long,
    @SerializedName("webformatHeight")
    val webformatHeight: Int,
    @SerializedName("webformatURL")
    val webformatURL: String,
    @SerializedName("webformatWidth")
    val webformatWidth: Int
)