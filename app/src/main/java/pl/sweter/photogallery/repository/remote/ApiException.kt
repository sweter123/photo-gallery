package pl.sweter.photogallery.repository.remote

class ApiException(val code: Int, message: String) : Exception(message) {
}