package pl.sweter.photogallery.repository.remote

import io.reactivex.Single
import io.reactivex.SingleSource
import io.reactivex.SingleTransformer
import pl.sweter.photogallery.BuildConfig
import retrofit2.Response
import java.util.*
import javax.inject.Inject

class NetworkPhotoRepository @Inject constructor(private val photosService: PhotosService) {

  fun getPhotos(
    page: Long,
    pageSize: Int,
    query: String?
  ) = photosService
    .getPhotos(
      apiKey = BuildConfig.API_KEY,
      langCode = Locale.getDefault().language,
      page = page,
      pageSize = pageSize,
      query = query
    )
    .compose(unpackAndHandleErrors())
    .map { it.hits }

  private fun <T> unpackAndHandleErrors() = SingleTransformer<Response<T>, T> {
    it.flatMap { response ->
      if (response.isSuccessful) {
        Single.just(response.body())
      } else {
        Single.error(ApiException(response.code(), response.message()))
      }
    }
  }

}
