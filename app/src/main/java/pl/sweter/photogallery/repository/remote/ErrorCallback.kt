package pl.sweter.photogallery.repository.remote

import androidx.annotation.StringRes

interface ErrorCallback {
  fun onInitialError(@StringRes message: Int)
  fun onNextPageError(@StringRes message: Int)
}